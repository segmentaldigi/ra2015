var map_do, map_office;

$(window).on('scroll', function () {
    if ($(window).scrollTop() >= 300 && !$('.body__navbar').hasClass('navbar-fixed-top')) {
        $('.body__navbar').removeClass('navbar-static-top').addClass('navbar-fixed-top');
    }
    else if ($(window).scrollTop() < 300 && $('.body__navbar').hasClass('navbar-fixed-top')) {
        $('.body__navbar').removeClass('navbar-fixed-top').addClass('navbar-static-top');
    }
});

$(function () {
    $(".lazyload").lazyload({
        effect: "fadeIn"
    });
});


//Speakers view switch
$('.view-switch>.btn-group>.btn').on('click',function(e){
    if($(this).hasClass('active')) return;
    var target = $(this).find('input').data('target');
    $(target).toggleClass('persons--text-list');
});

//Event Place filter
$('.place-switch>.btn-group>.btn').on('click',function(e){
    if($(this).hasClass('active')) return;
    var target = $(this).find('input').data('target');
    var attrib = $(this).find('input').data('attrib');
    var filter = $(this).find('input').data('filter');
    if(filter == 'all') {
        $(target).find(".filtering").not(".active").addClass("active");
        $(target).find(".event-list__period:hidden").slideDown("fast");
        return;
    }

    $(target).find(".filtering").not("[data-"+attrib+"='"+filter+"']").removeClass("active");
    $(target).find("[data-"+attrib+"='"+filter+"']").not(".active").addClass("active");
    $(target).find(".event-list__period").each(function(){
        var $period = $(this);
        var $events = $period.find('.event-list__item.active');
        if($events.length==0){
            $period.slideUp("fast");
        }
        else{
            $period.slideDown("fast");
        }
    });
});

$(document).ready(function () {

    //Tab activate by url hash
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if (history.pushState) {
            history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
        } else {
            location.hash = '#' + $(e.target).attr('href').substr(1);
        }
    });

    var wow = new WOW(
        {
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: false,
            live: true,
            callback: function (box) {
            }
        }
    );
    wow.init();


    //Sliders in content AUTO
    $('.slider--many-images').each(function () {
        $(this).owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplaySpeed: 1000,
            nav: true,
            margin: 30,
            navText: ['<i class="icon-aleft"></i>', '<i class="icon-aright"></i>'],
            navContainer: '.slider--many-images',
            dots: false,
            animateOut: 'fadeOut',
            responsive: {
                640: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    });

    $('.slider--one-image').each(function () {
        $(this).owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplaySpeed: 1000,
            nav: false,
            margin: 30,
            navText: ['<i class="icon-aleft"></i>', '<i class="icon-aright"></i>'],
            navContainer: '.slider--one-image',
            dots: true,
            animateOut: 'fadeOut',
        });
    });

    //Header slider activation
    if ($("#header_images").length) {
        var apt = 3000,
            aps = 1000,
            ao = 'fadeOut';
        if($("body").hasClass("fest")){
            var apt = 100,
                aps = 600,
                ao = 'fadeOutFest';
        }
        $("#header_images").owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: apt,
            autoplaySpeed: aps,
            nav: false,
            dots: false,
            animateOut: ao,
        });
    }

    //Speakers srolling Homepage
    if ($("#speakers_scroll").length > 0) {
        $("#speakers_scroll").owlCarousel({
            center: true,
            items: 2,
            loop: true,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: true,
            autoplaySpeed: 1000,
            navSpeed: 500,
            margin: 10,
            dotsEach: 2,
            navText: ['<i class="icon-aleft"></i>', '<i class="icon-aright"></i>'],
            navContainer: '#speakers_scroll',
            responsive: {
                640: {
                    items: 3,
                    center: true,
                    dots: true,
                    dotsEach: 3,
                },
                1000: {
                    items: 4,
                    center: true,
                    nav: true,
                    dots: false,
                    dotsEach: 4,
                },
                1300: {
                    items: 5,
                    center: true,
                    nav: true,
                    dots: false,
                    dotsEach: 5,
                },
                1600: {
                    items: 5,
                    center: true,
                    nav: true,
                    dots: false,
                    dotsEach: 5,
                }
            }
        });
    }


    if ($("#partners_scroll").length) {
        $("#partners_scroll").owlCarousel({
            center: false,
            items: 2,
            dots: false,
            loop: true,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,
            autoplaySpeed: 1000,
            navSpeed: 500,
            margin: 10,
            navText: ['<i class="icon-aleft"></i>', '<i class="icon-aright"></i>'],
            navContainer: '#partners_scroll',
            responsive: {
                640: {
                    items: 3,
                    center: true,
                },
                1000: {
                    items: 3,
                    center: true,
                },
                1300: {
                    items: 5,
                    center: true,
                },
                1600: {
                    items: 5,
                    center: true,
                }
            }
        });
    }


    if ($("#links_scroll").length > 0) {
        $("#links_scroll").owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            autoplaySpeed: 1000,
            navSpeed: 500,
            dots: false,
            nav: false
        });
    }


    //Digital October map activation
    if ($("#map_do").length > 0) {
        var mapLocation = new google.maps.LatLng(55.7411, 37.6092);
        map_do = new GMaps({
            div: '#map_do',
            center: mapLocation,
            zoom: 17,
            scrollwheel: false,
            styles: [
                {"stylers": [
                    {"saturation": -100},
                    {"gamma": 1}
                ]},
                {"elementType": "labels.text.stroke", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.business", "elementType": "labels.text", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.business", "elementType": "labels.icon", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.place_of_worship", "elementType": "labels.text", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.place_of_worship", "elementType": "labels.icon", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "road", "elementType": "geometry", "stylers": [
                    {"visibility": "simplified"}
                ]},
                {"featureType": "water", "stylers": [
                    {"visibility": "on"},
                    {"saturation": 50},
                    {"gamma": 0},
                    {"hue": "#50a5d1"}
                ]},
                {"featureType": "administrative.neighborhood", "elementType": "labels.text.fill", "stylers": [
                    {"color": "#333333"}
                ]},
                {"featureType": "road.local", "elementType": "labels.text", "stylers": [
                    {"weight": 0.5},
                    {"color": "#333333"}
                ]},
                {"featureType": "transit.station", "elementType": "labels.icon", "stylers": [
                    {"gamma": 1},
                    {"saturation": 50}
                ]}
            ]
        });

        var image = new google.maps.MarkerImage('assets/img/map/mapmarker.png',
            new google.maps.Size(51, 120),
            null,
            new google.maps.Point(25, 120)
        );

        map_do.addMarker({
            position: mapLocation,
            icon: image,
            title: 'Red Apple 2015',
            infoWindow: {
                content: '<p><strong>Digital October</strong><br/>Россия. Москва.<br/>Берсеневская набережная 6, стр.3</p>'
            }
        });
    }

    //Office map activation
    if ($("#map_office").length > 0) {
        var mapLocation = new google.maps.LatLng(55.7411, 37.6092);
        map_office = new GMaps({
            div: '#map_office',
            center: mapLocation,
            zoom: 17,
            scrollwheel: false,
            styles: [
                {"stylers": [
                    {"saturation": -100},
                    {"gamma": 1}
                ]},
                {"elementType": "labels.text.stroke", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.business", "elementType": "labels.text", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.business", "elementType": "labels.icon", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.place_of_worship", "elementType": "labels.text", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "poi.place_of_worship", "elementType": "labels.icon", "stylers": [
                    {"visibility": "off"}
                ]},
                {"featureType": "road", "elementType": "geometry", "stylers": [
                    {"visibility": "simplified"}
                ]},
                {"featureType": "water", "stylers": [
                    {"visibility": "on"},
                    {"saturation": 50},
                    {"gamma": 0},
                    {"hue": "#50a5d1"}
                ]},
                {"featureType": "administrative.neighborhood", "elementType": "labels.text.fill", "stylers": [
                    {"color": "#333333"}
                ]},
                {"featureType": "road.local", "elementType": "labels.text", "stylers": [
                    {"weight": 0.5},
                    {"color": "#333333"}
                ]},
                {"featureType": "transit.station", "elementType": "labels.icon", "stylers": [
                    {"gamma": 1},
                    {"saturation": 50}
                ]}
            ]
        });

        var image = new google.maps.MarkerImage('assets/img/map/mapmarker.png',
            new google.maps.Size(51, 120),
            null,
            new google.maps.Point(25, 120)
        );

        map_office.addMarker({
            position: mapLocation,
            icon: image,
            title: 'Red Apple 2015',
            infoWindow: {
                content: '<p><strong>Digital October</strong><br/>Россия. Москва.<br/>Берсеневская набережная 6, стр.3</p>'
            }
        });
    }


});

